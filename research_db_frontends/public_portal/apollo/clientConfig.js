import { InMemoryCache } from 'apollo-cache-inmemory'
export default function (_) {
  return {
    httpLinkOptions: {
      uri: 'https://api.rdb.dnk8n.dev/v1/graphql',
    },
    cache: new InMemoryCache(),
    wsEndpoint: 'wss://api.rdb.dnk8n.dev/v1/graphql',
  }
}
