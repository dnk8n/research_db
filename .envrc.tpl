export PROJECT_NAME=rdb
export DEPLOYMENT_ENVIRONMENT=develop  # develop, staging, production
export PROJECT_ROOT=${PWD}
export PATH="${PROJECT_ROOT}:${PATH}"

# Cloud Provider Secrets
export HCLOUD_TOKEN=
export VAGRANT_IP=10.180.0.100

# Cloud Provider Variables
export HCLOUD_IMAGE=ubuntu-20.04
export HCLOUD_LOCATION=nbg1
export HCLOUD_SERVER_TYPE=cpx11
export HCLOUD_SNAPSHOT_NAME=rdb-base
export ROOT_SSH_USER=root
export PRIMARY_SSH_USER=ubuntu

# Container Secrets
export ACME_EMAIL=
export CF_API_EMAIL=
export CF_API_KEY=
export TRAEFIK_BASICAUTH_USERS=''

# Container Variables
export VIRTUALIZATION=$(systemd-detect-virt -v)
export DOMAIN=rdb.lan
export DOMAIN_DJANGO=admin.${DOMAIN}
export DOMAIN_HASURA=api.${DOMAIN}
export DOMAIN_TRAEFIK=router.${DOMAIN}
export DOMAIN_NETDATA=monitor.${DOMAIN}
export DOMAIN_PROMETHEUS=metrics.${DOMAIN}
export DOMAIN_GRAFANA=dash.${DOMAIN}
export DOMAIN_MAILHOG=mail.${DOMAIN}

# Application Secrets
export POSTGRES_PASSWORD=rdb
export DJANGO_SECRET_KEY=rdb
export HASURA_GRAPHQL_ADMIN_SECRET=rdb
export EMAIL_HOST=mailhog
export EMAIL_PORT=1025
export EMAIL_HOST_USER=
export EMAIL_HOST_PASSWORD=

# Application Variables
export POSTGRES_USER=rdb
export POSTGRES_HOST=postgres
export POSTGRES_PORT=5432
export POSTGRES_DB=rdb
export DATABASE_URL=postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_DB}
export REDIS_URL=redis://redis:6379
export DJANGO_IMAGE=rdb
export DJANGO_IMAGE_TAG=$(git branch --show-current)
