"""
Django settings for research_db project.

Generated by 'django-admin startproject' using Django 3.0.5.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.0/ref/settings/
"""

import logging
import logging.config
import multiprocessing
from pathlib import Path

import environ
from configurations import Configuration


class Common(Configuration):

    BASE_DIR = (Path(__file__).parent / "../..").resolve()

    env = environ.Env()

    # See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/
    DEBUG = False
    DATABASES = {"default": env.db()}

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
    INSTALLED_APPS = [
        "object_tools",
        "dal",
        "dal_select2",
        "django.contrib.admin",
        "django.contrib.auth",
        "django.contrib.contenttypes",
        "django.contrib.sessions",
        "django.contrib.messages",
        "whitenoise.runserver_nostatic",
        "django.contrib.staticfiles",
        "django_q",
        "django_better_admin_arrayfield",
        "easyaudit",
        "apps.research_db",
        "apps.grid",
    ]

    MIDDLEWARE = [
        "django.middleware.security.SecurityMiddleware",
        "whitenoise.middleware.WhiteNoiseMiddleware",
        "django.contrib.sessions.middleware.SessionMiddleware",
        "django.middleware.common.CommonMiddleware",
        "django.middleware.csrf.CsrfViewMiddleware",
        "django.contrib.auth.middleware.AuthenticationMiddleware",
        "django.contrib.messages.middleware.MessageMiddleware",
        "django.middleware.clickjacking.XFrameOptionsMiddleware",
        "easyaudit.middleware.easyaudit.EasyAuditMiddleware",
    ]

    ROOT_URLCONF = "config.urls"

    TEMPLATES = [
        {
            "BACKEND": "django.template.backends.django.DjangoTemplates",
            "DIRS": [],
            "APP_DIRS": True,
            "OPTIONS": {
                "context_processors": [
                    "django.template.context_processors.debug",
                    "django.template.context_processors.request",
                    "django.contrib.auth.context_processors.auth",
                    "django.contrib.messages.context_processors.messages",
                ],
            },
        },
    ]

    WSGI_APPLICATION = "config.wsgi.app"

    # Password validation
    # https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

    AUTH_PASSWORD_VALIDATORS = [
        {
            "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
        },
        {
            "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
        },
        {
            "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
        },
        {
            "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
        },
    ]

    # Internationalization
    # https://docs.djangoproject.com/en/3.0/topics/i18n/

    LANGUAGE_CODE = "en-us"

    TIME_ZONE = "UTC"

    USE_I18N = True

    USE_L10N = True

    USE_TZ = True

    # Static files (CSS, JavaScript, Images)
    # https://docs.djangoproject.com/en/3.0/howto/static-files/

    STATIC_URL = "/static/"
    STATIC_ROOT = "/static"

    logging.config.dictConfig(
        {
            "version": 1,
            "disable_existing_loggers": True,
            "root": {
                "level": "INFO",
                "handlers": ["console"],
            },
            "formatters": {
                "verbose": {
                    "format": "%(levelname)s %(asctime)s %(module)s %(message)s"
                },
            },
            "handlers": {
                "console": {
                    "level": "DEBUG",
                    "class": "logging.StreamHandler",
                    "formatter": "verbose",
                }
            },
            "loggers": {
                "django.db.backends": {
                    "level": "ERROR",
                    "handlers": ["console"],
                    "propagate": False,
                },
                "django.security.DisallowedHost": {
                    "level": "ERROR",
                    "handlers": ["console"],
                    "propagate": False,
                },
            },
        }
    )
    logger = logging.getLogger(__name__)

    STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"

    Q_CLUSTER = {
        "name": "research_db",
        "workers": max(multiprocessing.cpu_count() - 1, 1),
        "redis": env.str("REDIS_URL", default=None),
        "save_limit": 0,
    }

    EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
    DEFAULT_FROM_EMAIL = "Research Database <info@dnk8n.dev>"
    SERVER_EMAIL = "info@dnk8n.dev"
    ADMINS = [("info", "info@dnk8n.dev"), ("Dean", "deankayton@gmail.com")]
    EMAIL_HOST = env.str("EMAIL_HOST")
    EMAIL_PORT = env.int("EMAIL_PORT")
    EMAIL_HOST_USER = env.str("EMAIL_HOST_USER")
    EMAIL_HOST_PASSWORD = env.str("EMAIL_HOST_PASSWORD")

    DJANGO_EASY_AUDIT_UNREGISTERED_CLASSES_EXTRA = [
        "django_q.Failure",
        "django_q.OrmQ",
        "django_q.Schedule",
        "django_q.Success",
        "django_q.Task",
    ]
