from config.settings.common import Common


class Develop(Common):
    ALLOWED_HOSTS = ["*"]
    DEBUG = True
    SECRET_KEY = "dummy"
