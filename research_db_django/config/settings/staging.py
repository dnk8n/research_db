from config.settings.common import Common


class Staging(Common):
    ALLOWED_HOSTS = Common.env.list("ALLOWED_HOSTS")
    DEBUG = False
    SECRET_KEY = Common.env.str("SECRET_KEY")
