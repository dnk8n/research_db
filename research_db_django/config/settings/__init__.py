import os

CONFIG = os.getenv("DJANGO_CONFIGURATION")

if CONFIG == "Develop":
    from .develop import Develop
elif CONFIG == "Staging":
    from .staging import Staging
