"""
ASGI config for research_db project.

It exposes the ASGI callable as a module-level variable named ``app``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/asgi/
"""

import os

from configurations import importer

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
os.environ.setdefault("DJANGO_CONFIGURATION", "Develop")
importer.install()

from django.core.asgi import get_asgi_application  # isort:skip

app = get_asgi_application()
