"""research_db URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import object_tools
from django.conf.urls import url
from django.contrib import admin
from django.urls import path

from apps.research_db import views

urlpatterns = [
    path("admin/", admin.site.urls),
    path("object-tools/", object_tools.tools.urls),
    url(
        r"^keywords-select/$",
        views.KeywordsSelect.as_view(),
        name="keywords-select",
    ),
    url(
        r"^countries-select/$",
        views.CountriesSelect.as_view(),
        name="countries-select",
    ),
]
