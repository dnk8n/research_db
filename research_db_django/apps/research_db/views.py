from dal import autocomplete

from apps.research_db import models


class KeywordsSelect(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return models.Keyword.objects.none()
        qs = models.Keyword.objects.all()
        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs

    def get_result_value(self, result):
        return result.pk


class CountriesSelect(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return models.Country.objects.none()
        qs = models.Country.objects.all()
        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs

    def get_result_value(self, result):
        return result.pk
