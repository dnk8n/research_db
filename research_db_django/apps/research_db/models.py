import logging

from django.contrib.postgres.fields import JSONField
from django.db import models
from django_better_admin_arrayfield.models.fields import ArrayField

logger = logging.getLogger(__name__)


class GridRelease(models.Model):
    version = models.DateField(primary_key=True, editable=False)
    data = JSONField()
    urls = JSONField()
    md5 = models.CharField(max_length=32, unique=True)

    def __str__(self):
        return str(self.version)


class Keyword(models.Model):
    name = models.TextField()

    def __str__(self):
        return str(self.name)


class Region(models.Model):
    region_code = models.IntegerField(unique=True)
    name = models.TextField(unique=True)

    def __str__(self):
        return str(self.name)


class SubRegion(models.Model):
    sub_region_code = models.IntegerField(unique=True)
    name = models.TextField(unique=True)
    region = models.ForeignKey(
        Region,
        on_delete=models.PROTECT,
        related_name="subregion_region",
        blank=True,
        null=True,
    )

    def __str__(self):
        return str(self.name)


class IntermediateRegion(models.Model):
    intermediate_region_code = models.IntegerField(unique=True)
    name = models.TextField(unique=True)
    sub_region = models.ForeignKey(
        SubRegion,
        on_delete=models.PROTECT,
        related_name="intermediateregion_subregion",
        blank=True,
        null=True,
    )

    def __str__(self):
        return str(self.name)


class Country(models.Model):
    """
    More info: https://en.wikipedia.org/wiki/ISO_3166-1
    """

    country_code = models.IntegerField(unique=True, null=True)
    name = models.TextField(unique=True)
    alpha_2_code = models.CharField(max_length=2, unique=True)
    alpha_3_code = models.CharField(max_length=3, unique=True, null=True)
    keywords = models.ManyToManyField(Keyword, blank=True)
    region = models.ForeignKey(
        Region,
        on_delete=models.PROTECT,
        related_name="country_region",
        blank=True,
        null=True,
    )
    sub_region = models.ForeignKey(
        SubRegion,
        on_delete=models.PROTECT,
        related_name="country_subregion",
        blank=True,
        null=True,
    )
    intermediate_region = models.ForeignKey(
        IntermediateRegion,
        on_delete=models.PROTECT,
        related_name="country_intermediateregion",
        blank=True,
        null=True,
    )

    class Meta:
        verbose_name_plural = "Countries"

    def __str__(self):
        return str(self.name)


class Category(models.Model):
    name = models.TextField()

    class Meta:
        verbose_name_plural = "Categories"

    def __str__(self):
        return str(self.name)


class AcademicPosition(models.Model):
    name = models.TextField()

    def __str__(self):
        return str(self.name)


class Usage(models.Model):
    name = models.TextField()

    def __str__(self):
        return str(self.name)


class ThematicFocus(models.Model):
    name = models.TextField()

    class Meta:
        verbose_name_plural = "Thematic focuses"

    def __str__(self):
        return str(self.name)


class FundingOrganization(models.Model):
    grid_institute = models.OneToOneField(
        'grid.GridInstitute', on_delete=models.DO_NOTHING, null=True, blank=True
    )
    division = models.TextField(null=True, blank=True)
    sub_division = models.TextField(null=True, blank=True)
    is_funder = models.BooleanField(null=True, blank=True)
    note = models.TextField(null=True, blank=True)
    keywords = models.ManyToManyField(Keyword)

    def __str__(self):
        return str(self.grid_institute)


class FundingProgram(models.Model):
    title = models.TextField()
    funded_by = models.ManyToManyField(FundingOrganization, related_name="funded")
    implemented_by = models.ManyToManyField(
        FundingOrganization, related_name="implemented", blank=True
    )
    url_links = ArrayField(models.URLField())
    document_links = ArrayField(models.TextField(), null=True, blank=True)
    deadline = models.DateTimeField()
    usage = models.ManyToManyField(Usage)
    thematic_focus = models.ManyToManyField(ThematicFocus, blank=True)
    applicant_countries = models.ManyToManyField(Country, blank=True)
    academic_position = models.ManyToManyField(AcademicPosition)
    category = models.ManyToManyField(Category)
    keywords = models.ManyToManyField(Keyword)
    description = models.TextField()
    eligibility = models.TextField()
    partner_necessary = models.BooleanField(choices=((True, "Yes"), (False, "No")))

    def __str__(self):
        return str(self.title)
