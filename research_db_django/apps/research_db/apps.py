from django.apps import AppConfig


class ResearchDbConfig(AppConfig):
    name = "research_db"
