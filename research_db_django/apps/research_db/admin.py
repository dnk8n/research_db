import object_tools
from dal import autocomplete
from django import forms
from django.contrib import admin
from django.http import HttpResponse
from django_better_admin_arrayfield.admin.mixins import DynamicArrayMixin
from openpyxl.writer.excel import save_virtual_workbook

from apps.research_db import models


class NoDeleteAdminMixin:
    def has_delete_permission(self, request, obj=None):
        return False


class NoChangeAdminMixin:
    def has_change_permission(self, request, obj=None):
        return False


class NoAddAdminMixin:
    def has_add_permission(self, request, obj=None):
        return False


class CountryInline(admin.TabularInline):
    model = models.Country.keywords.through


class FundingOrganizationInline(admin.TabularInline):
    model = models.FundingOrganization.keywords.through


class FundingProgramInline(admin.TabularInline):
    model = models.FundingProgram.keywords.through


@admin.register(models.Keyword)
class KeywordAdmin(admin.ModelAdmin):
    inlines = (CountryInline, FundingOrganizationInline, FundingProgramInline)
    search_fields = ("name",)


@admin.register(models.Country)
class CountryAdmin(NoAddAdminMixin, NoDeleteAdminMixin, admin.ModelAdmin):
    list_display = (
        "name",
        "country_code",
        "alpha_2_code",
        "alpha_3_code",
        "region",
        "sub_region",
        "intermediate_region",
    )
    list_filter = ("region", "sub_region", "intermediate_region", "keywords")
    search_fields = (
        "name",
        "country_code",
        "alpha_2_code",
        "alpha_3_code",
        "keywords__name",
        "region",
        "sub_region",
        "intermediate_region",
    )
    readonly_fields = (
        "name",
        "country_code",
        "alpha_2_code",
        "alpha_3_code",
        "region",
        "sub_region",
        "intermediate_region",
    )
    autocomplete_fields = ("keywords",)
    ordering = ("name",)


@admin.register(models.Category)
class CategoryAdmin(admin.ModelAdmin):
    search_fields = ("name",)


@admin.register(models.AcademicPosition)
class AcademicPositionAdmin(admin.ModelAdmin):
    search_fields = ("name",)


@admin.register(models.Usage)
class UsageAdmin(admin.ModelAdmin):
    search_fields = ("name",)


@admin.register(models.ThematicFocus)
class ThematicFocusAdmin(admin.ModelAdmin):
    search_fields = ("name",)


@admin.register(models.FundingOrganization)
class FundingOrganizationAdmin(admin.ModelAdmin):
    autocomplete_fields = ("keywords",)
    search_fields = (
        "grid_institute__grid_id",
        "grid_institute__name",
        "grid_institute__aliases__name",
        "grid_institute__acronyms__name",
        "grid_institute__addresses__country__name",
        "grid_institute__addresses__city",
        "grid_institute__addresses__country__alpha_2_code",
        "grid_institute__addresses__country__alpha_3_code",
        "keywords__name",
    )
    raw_id_fields = ("grid_institute",)


class FundingProgramForm(forms.ModelForm):
    bulk_add_countries_by_keyword = forms.ModelMultipleChoiceField(
        queryset=models.Keyword.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(url="keywords-select"),
        required=False,
    )

    class Meta:
        widgets = {
            "applicant_countries": autocomplete.ModelSelect2Multiple(
                url="countries-select"
            )
        }

    def save(self, commit=True):
        qs = self.cleaned_data["applicant_countries"].union(
            models.Country.objects.filter(
                keywords__in=self.cleaned_data["bulk_add_countries_by_keyword"]
            )
        )
        self.cleaned_data["applicant_countries"] = qs
        return super(FundingProgramForm, self).save(commit=commit)


@admin.register(models.FundingProgram)
class FundingProgramAdmin(admin.ModelAdmin, DynamicArrayMixin):
    form = FundingProgramForm
    fields = (
        "title",
        "funded_by",
        "implemented_by",
        "url_links",
        "document_links",
        "deadline",
        "usage",
        "thematic_focus",
        (
            "applicant_countries",
            "bulk_add_countries_by_keyword",
        ),
        "academic_position",
        "category",
        "keywords",
        "description",
        "eligibility",
        "partner_necessary",
    )
    autocomplete_fields = (
        "funded_by",
        "implemented_by",
        "usage",
        "thematic_focus",
        "academic_position",
        "category",
        "keywords",
    )
    search_fields = (
        "funded_by__grid_institute__grid_id",
        "funded_by__grid_institute__name",
        "funded_by__grid_institute__aliases__name",
        "funded_by__grid_institute__acronyms__name",
        "funded_by__grid_institute__addresses__country__name",
        "funded_by__grid_institute__addresses__city",
        "funded_by__grid_institute__addresses__country__alpha_2_code",
        "funded_by__grid_institute__addresses__country__alpha_3_code",
        "funded_by__keywords__name",
        "implemented_by__grid_institute__grid_id",
        "implemented_by__grid_institute__name",
        "implemented_by__grid_institute__aliases__name",
        "implemented_by__grid_institute__acronyms__name",
        "implemented_by__grid_institute__addresses__country__name",
        "implemented_by__grid_institute__addresses__city",
        "implemented_by__grid_institute__addresses__country__alpha_2_code",
        "implemented_by__grid_institute__addresses__country__alpha_3_code",
        "implemented_by__keywords__name",
        "applicant_countries__name",
        "applicant_countries__alpha_2_code",
        "applicant_countries__alpha_3_code",
        "usage__name",
        "thematic_focus__name",
        "academic_position__name",
        "category__name",
        "keywords__name",
        "applicant_countries__keywords__name",
    )
