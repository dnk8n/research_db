import hashlib
import io
import json
import logging
import sys
import zipfile
from datetime import datetime

import requests
from bs4 import BeautifulSoup
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.urls import reverse
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from openpyxl import Workbook, load_workbook
from openpyxl.styles import PatternFill, colors
from openpyxl.utils import get_column_letter
from pygments import highlight
from pygments.formatters import HtmlFormatter
from pygments.lexers import YamlLexer
from ruamel import yaml

# TODO: Don't import these here, these should be passed in as parameters
from apps.research_db.models import (
    FundingOrganization,
    GridRelease,
)

logger = logging.getLogger(__name__)


def download_grid_release():
    logger.info("Downloading...")
    (
        grid_md5,
        grid_home_url,
        grid_article_url,
        grid_download_url,
    ) = grid_release_field_metadata()
    installed_grid_is_latest = get_latest_installed_grid_release(grid_md5)
    if installed_grid_is_latest:
        return installed_grid_is_latest
    else:
        return create_grid_release(
            get_grid_json(grid_md5, grid_download_url),
            grid_md5,
            grid_home_url,
            grid_article_url,
            grid_download_url,
        )


def grid_release_field_metadata():
    grid_home_url = "https://www.grid.ac/downloads"
    grid_home_response = get_grid_home(grid_home_url)
    grid_article_url = parse_grid_article_url(grid_home_response)
    grid_article_response = get_grid_article(grid_article_url)
    grid_article_scripts = get_grid_article_scripts(grid_article_response)
    grid_download_url, grid_md5 = parse_grid_download_url_and_md5(grid_article_scripts)
    return grid_md5, grid_home_url, grid_article_url, grid_download_url


def get_validated_grid_json(grid_download_response, grid_supplied_md5):
    grid_file_bytes = io.BytesIO(grid_download_response.content)
    grid_file_md5 = hashlib.md5(grid_file_bytes.read()).hexdigest()
    if grid_file_md5 != grid_supplied_md5:
        logger.error("Response checksum did not match expected value")
        sys.exit(1)
    else:
        logger.debug("Response checksum matched expected value")
    grid_file_zip = zipfile.ZipFile(grid_file_bytes)
    for grid_file_path in grid_file_zip.namelist():
        if "grid.json" in grid_file_path:
            return json.loads(grid_file_zip.read(grid_file_path))


def get_grid_json(grid_md5, grid_download_url):
    grid_download_response = get_grid_download(grid_download_url)
    return get_validated_grid_json(grid_download_response, grid_md5)


def create_grid_release(
    grid_json, md5, grid_home_url, grid_article_url, grid_download_url
):
    logger.info(f"Creating GRID release version: {grid_json.get('version')}")
    latest_grid = GridRelease.objects.create(
        version=datetime.strptime(grid_json["version"], "release_%Y_%m_%d"),
        data=grid_json,
        md5=md5,
        urls={
            "grid_homepage": grid_home_url,
            "grid_article": grid_article_url,
            "grid_download": grid_download_url,
        },
    )
    logger.info(f"Successful.")
    return latest_grid


def get_latest_installed_grid_release(grid_supplied_md5=None):
    try:
        latest_installed_grid = GridRelease.objects.latest("version")
    except ObjectDoesNotExist as e:
        logger.warning(f"No existing Grid releases, {e}")
    else:
        if not grid_supplied_md5:
            logger.info("Latest GRID release returned. Not checking MD5 checksum...")
            return latest_installed_grid
        elif latest_installed_grid.md5 == grid_supplied_md5:
            logger.info(
                "Latest GRID release is already fetched. MD5 checksum values checked. Skipping download..."
            )
            return latest_installed_grid
    logger.debug("Latest GRID release is not fetched yet. Continuing...")
    return None


def get_grid_download(grid_download_url):
    grid_download_response = requests.get(grid_download_url)
    if grid_download_response.status_code == 200:
        return grid_download_response
    else:
        logger.error(
            f"Invalid response status code, "
            f"{grid_download_response.status_code} while "
            f"calling {grid_download_url}"
        )
        sys.exit(1)


def parse_grid_download_url_and_md5(scripts):
    for script in scripts:
        if (
            script.attrs.get("id") == "app-data"
            and script.attrs.get("type") == "text/json"
        ):
            app_data = json.loads(script.string)
            try:
                grid_file = app_data["article"]["files"][0]
            except KeyError as e:
                logger.exception(e)
            except IndexError as e:
                logger.exception(e)
            else:
                grid_download_url = grid_file["downloadUrl"]
                grid_supplied_md5 = grid_file["md5"]
                return grid_download_url, grid_supplied_md5


def parse_grid_article_url(grid_response):
    grid_soup = BeautifulSoup(grid_response.text, features="html.parser")
    text_links = {
        a.text: a["href"] for a in grid_soup.find_all("a", href=True) if a.text
    }
    latest_release_text = "Download latest release"
    grid_article_url = text_links.get(latest_release_text)
    if grid_article_url:
        return grid_article_url
    else:
        logger.error(
            f"Unable to locate latest release download link from "
            f"{grid_response.url}. Does '{latest_release_text}' button still "
            f"exist on page?"
        )
        sys.exit(1)


def get_grid_article(grid_article_url):
    grid_article_response = requests.get(grid_article_url)
    if grid_article_response.status_code == 200:
        return grid_article_response
    else:
        logger.error(
            f"Invalid response status code, {grid_article_response.status_code} while calling {grid_article_url}"
        )
        sys.exit(1)


def get_grid_article_scripts(grid_article_response):
    grid_article_soup = BeautifulSoup(
        grid_article_response.text, features="html.parser"
    )
    return grid_article_soup.find_all("script")


def get_grid_home(grid_home_url):
    grid_response = requests.get(grid_home_url)
    if grid_response.status_code == 200:
        return grid_response
    else:
        logger.error(
            f"Invalid response status code, {grid_response.status_code} while calling {grid_home_url}"
        )
        sys.exit(1)


def install_grid_references(grid_release):
    logger.info("Installing...")
    if not grid_release:
        grid_release = get_latest_installed_grid_release()
    grid_version = grid_release.version
    with transaction.atomic():
        GridReference.objects.all().delete()
        GridReference.objects.bulk_create(
            (
                GridReference(
                    id=institute["id"],
                    data=institute,
                    version=grid_version,
                )
                for institute in grid_release.data["institutes"]
            )
        )
