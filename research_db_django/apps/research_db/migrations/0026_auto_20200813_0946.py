import json
import logging

from django.conf import settings
from django.db import migrations
from django.db.models import Q

logger = logging.getLogger(__name__)


def forwards_func(apps, schema_editor):
    db_alias = schema_editor.connection.alias
    region_model = apps.get_model("research_db", "Region")
    subregion_model = apps.get_model("research_db", "SubRegion")
    intermediateregion_model = apps.get_model("research_db", "IntermediateRegion")
    country_model = apps.get_model("research_db", "Country")
    countries_json_path = (
        settings.BASE_DIR
        / "apps"
        / "research_db"
        / "data"
        / "iso-3166-countries-with-regional-codes.json"
    )
    with countries_json_path.open() as countries_json:
        countries = json.load(countries_json)
    for country in countries:
        logger.debug(
            f"Processing {country['name']} (country_code={country['country-code']}, alpha_2_code={country['alpha-2']}, alpha_3_code={country['alpha-3']})."
        )
        if country["region"]:
            region, _ = region_model.objects.using(db_alias).get_or_create(
                region_code=(int(country["region-code"])), name=country["region"]
            )
        else:
            region = None
        if country["sub-region"]:
            subregion, _ = subregion_model.objects.using(db_alias).get_or_create(
                sub_region_code=(int(country["sub-region-code"])),
                name=country["sub-region"],
                region=region,
            )
        else:
            subregion = None
        if country["intermediate-region"]:
            intermediateregion, _ = intermediateregion_model.objects.using(
                db_alias
            ).get_or_create(
                intermediate_region_code=(int(country["intermediate-region-code"])),
                name=country["intermediate-region"],
                sub_region=subregion,
            )
        else:
            intermediateregion = None
        country_qs = country_model.objects.using(db_alias).filter(
            Q(alpha_2_code=country["alpha-2"]) | Q(alpha_3_code=(country["alpha-3"]))
        )
        if len(country_qs) == 0:
            country_model.objects.using(db_alias).create(
                country_code=int(country["country-code"]),
                alpha_2_code=country["alpha-2"],
                alpha_3_code=country["alpha-3"],
                name=country["name"],
                region=region,
                sub_region=subregion,
                intermediate_region=intermediateregion,
            )
        elif len(country_qs) > 1:
            logger.error(
                f"Country queryset should have only one item. Data integrity issue. {country_qs}"
            )
        else:
            country_qs.update(
                country_code=int(country["country-code"]),
                alpha_2_code=country["alpha-2"],
                alpha_3_code=country["alpha-3"],
                name=country["name"],
                region=region,
                sub_region=subregion,
                intermediate_region=intermediateregion,
            )


class Migration(migrations.Migration):
    dependencies = [("research_db", "0025_auto_20200815_1920")]
    operations = [migrations.RunPython(forwards_func)]
