import json
from pathlib import Path

from django.conf import settings

json_path = settings.BASE_DIR / "apps/grid/data/grid.json"

with json_path.open() as json_content:
    data = json.load(json_content)

for inst in data["institutes"]:
    this_inst = list(inst.keys())
    if this_inst == ["id", "status", "redirect"]:
        assert inst["status"] == "redirected"
    elif this_inst == ["id", "status"]:
        assert inst["status"] == "obsolete"
    else:
        assert inst["status"] == "active"
        assert type(inst["name"]) == type(str())  # or type(inst['name']) == type(None)
        assert type(inst["wikipedia_url"]) == type(str()) or type(
            inst["wikipedia_url"]
        ) == type(None)
        assert type(inst["email_address"]) == type(str()) or type(
            inst["email_address"]
        ) == type(None)
        assert type(inst["links"]) == type(
            list()
        )  ### or type(inst['links']) == type(None)
        assert type(inst["aliases"]) == type(
            list()
        )  ### or type(inst['aliases']) == type(None)
        assert type(inst["acronyms"]) == type(
            list()
        )  ### or type(inst['acronyms']) == type(None)
        assert type(inst["types"]) == type(
            list()
        )  ### or type(inst['types']) == type(None)
        assert (
            inst["ip_addresses"] == list()
        )  ### or type(inst['ip_addresses']) == type(None)
        assert type(inst["addresses"]) == type(
            list()
        )  # or type(inst['addresses']) == type(None)
        assert len(inst["addresses"]) == 1
        assert type(inst["labels"]) == type(
            list()
        )  # or type(inst['labels']) == type(None)
        for label in inst["labels"]:
            assert list(label.keys()) == ["label", "iso639"]
        assert type(inst["id"]) == type(str())  # or type(inst['id']) == type(None)
        assert type(inst["status"]) == type(
            str()
        )  # or type(inst['status']) == type(None)
        assert type(inst["established"]) == type(int()) or type(
            inst["established"]
        ) == type(None)
        assert type(inst["relationships"]) == type(
            list()
        )  ### or type(inst['relationships']) == type(None)
        for relationship in inst["relationships"]:
            assert list(relationship.keys()) == ["type", "label", "id"]
            assert relationship["type"] in ["Child", "Parent", "Related", "Other"]
        assert type(inst.get("external_ids")) == type(dict()) or type(
            inst.get("external_ids")
        ) == type(None)
        assert type(inst["weight"]) == type(
            int()
        )  ### or type(inst['weight']) == type(None)
        assert inst["weight"] == 1
    print(f"{inst['id']} checked!")