import json
import sys
import uuid
from datetime import datetime

from django.conf import settings
from django.core.management import BaseCommand
from django.core.mail import mail_admins
from django.db.models import signals
from django_q.models import Schedule
from django_q.tasks import async_task, result_group, schedule
from easyaudit.signals import model_signals

from apps.grid import models
from apps.research_db.models import Country
from apps.utils import PauseSignals

signals_to_pause = PauseSignals(
    (
        (
            signals.post_save,
            model_signals.post_save,
            "easy_audit_signals_post_save",
        ),
        (
            signals.pre_save,
            model_signals.pre_save,
            "easy_audit_signals_pre_save",
        ),
        (
            signals.m2m_changed,
            model_signals.m2m_changed,
            "easy_audit_signals_m2m_changed",
        ),
        (
            signals.post_delete,
            model_signals.post_delete,
            "easy_audit_signals_post_delete",
        ),
    )
)


def import_grid_institutes(json_path):
    json_path = settings.BASE_DIR / json_path
    with json_path.open() as json_content:
        grid_data = json.load(json_content)

    job_suffix = str(uuid.uuid4())[:8]
    group_id = (
        f"import_grid_institutes_{datetime.today().strftime('%Y-%m-%d')}_{job_suffix}"
    )
    # for grid_idx, grid_institute in enumerate(grid_data["institutes"], 1):
    for grid_institute in grid_data["institutes"]:
        task_id = f"{grid_institute['id']}_{job_suffix}"
        q_options = {"ack_failure": True, "group": group_id, "task_name": task_id}
        with signals_to_pause:
            async_task(
                "apps.grid.tasks.import_grid_institute",
                grid_institute,
                q_options=q_options,
            )
    # result_group(group_id, failures=True, count=grid_idx)
    # msg = f"{grid_idx} institutes processed!"
    # mail_admins(msg, "Some text here")
    # return msg


# schedule(
#     "apps.grid.tasks.import_grid_institutes",
#     hook="apps.grid.hooks.print_result",
#     q_options={
#         "ack_failure": True,
#         "group": "import_grid_institutes",
#         "task_name": f"import_grid_institutes_{datetime.today().strftime('%Y-%m-%d')}"
#     },
#     schedule_type=Schedule.DAILY
# )


def import_grid_institute(inst):
    with signals_to_pause:
        if inst.get("status") == "redirected":
            grid_id_redirect, _ = models.GridInstitute.objects.get_or_create(
                grid_id=inst.get("redirect"), defaults={"status": "inactive"}
            )
            grid_institute, _ = models.GridInstitute.objects.update_or_create(
                grid_id=inst.get("id"),
                defaults={"status": inst.get("status"), "redirect": grid_id_redirect},
            )
        elif inst.get("status") == "obsolete":
            grid_institute, _ = models.GridInstitute.objects.update_or_create(
                grid_id=inst.get("id"), defaults={"status": inst.get("status")}
            )
        else:
            links = []
            for link in inst.get("links", []):
                grid_link, _ = models.GridLink.objects.update_or_create(name=link)
                links.append(grid_link)
            aliases = []
            for alias in inst.get("aliases", []):
                grid_alias, _ = models.GridAlias.objects.update_or_create(name=alias)
                aliases.append(grid_alias)
            acronyms = []
            for acronym in inst.get("acronyms", []):
                grid_acronym, _ = models.GridAcronym.objects.update_or_create(
                    name=acronym
                )
                acronyms.append(grid_acronym)
            types = []
            for type_ in inst.get("types", []):
                grid_type, _ = models.GridType.objects.update_or_create(name=type_)
                types.append(grid_type)
            ip_addresses = []
            for ip_address in inst.get("ip_addresses", []):
                grid_ip_address, _ = models.GridIpAddress.objects.update_or_create(
                    name=ip_address
                )
                ip_addresses.append(grid_ip_address)
            addresses = []
            for address in inst.get("addresses", []):
                if address.get("country_code"):
                    country, _ = Country.objects.get_or_create(
                        alpha_2_code=address.get("country_code"),
                        defaults={"name": address.get("country")},
                    )
                elif address.get("country"):
                    country = Country.objects.get(name=address.get("country"))
                else:
                    # Assume country name exists and matches at least
                    pass
                geonames_admins = []
                geonames_admin_objs = []
                if address.get("geonames_city"):
                    if address["geonames_city"]["geonames_admin1"]:
                        geonames_admin_objs.append(
                            address["geonames_city"]["geonames_admin1"]
                        )
                    if address["geonames_city"]["geonames_admin2"]:
                        geonames_admin_objs.append(
                            address["geonames_city"]["geonames_admin2"]
                        )
                    for geonames_admin_obj in geonames_admin_objs:
                        (
                            geonames_admin,
                            _,
                        ) = models.GeonamesAdmin.objects.update_or_create(
                            code=geonames_admin_obj["code"],
                            defaults={
                                "name": geonames_admin_obj["name"],
                                "ascii_name": geonames_admin_obj["ascii_name"],
                            },
                        )
                        geonames_admins.append(geonames_admin)
                    _license, _ = models.License.objects.update_or_create(
                        attribution=address["geonames_city"]["license"]["attribution"],
                        license=address["geonames_city"]["license"]["license"],
                    )
                    geonames_city, _ = models.GeonamesCity.objects.update_or_create(
                        geonames_city_id=address["geonames_city"]["id"],
                        defaults={
                            "city": address["geonames_city"]["city"],
                            "nuts_level1": address["geonames_city"]["id"],
                            "nuts_level2": address["geonames_city"]["nuts_level1"],
                            "nuts_level3": address["geonames_city"]["nuts_level3"],
                            "license": _license,
                        },
                    )
                    geonames_city.geonames_admin.set(geonames_admins)
                else:
                    geonames_city = None
                if address.get("lat") and address.get("lng"):
                    _address, _ = models.GridAddress.objects.update_or_create(
                        lat=address.get("lat"),
                        lng=address.get("lng"),
                        defaults={
                            "line_1": address.get("line_1"),
                            "line_2": address.get("line_2"),
                            "line_3": address.get("line_3"),
                            "postcode": address.get("postcode"),
                            "primary": address.get("primary", True),
                            "city": address.get("city", ""),
                            "state": address.get("state"),
                            "state_code": address.get("state_code"),
                            "country": country,
                            "geonames_city": geonames_city,
                        },
                    )
                else:
                    _address, _ = models.GridAddress.objects.update_or_create(
                        lat=address.get("lat"),
                        lng=address.get("lng"),
                        line_1=address.get("line_1"),
                        line_2=address.get("line_2"),
                        line_3=address.get("line_3"),
                        postcode=address.get("postcode"),
                        primary=address.get("primary", True),
                        city=address.get("city", ""),
                        state=address.get("state"),
                        state_code=address.get("state_code"),
                        country=country,
                        geonames_city=geonames_city,
                    )
                addresses.append(_address)
            labels = []
            for label in inst.get("labels", []):
                iso639, _ = models.Iso639.objects.update_or_create(name=label["iso639"])
                grid_label, _ = models.GridLabel.objects.update_or_create(
                    label=label.get("label"), iso639=iso639
                )
                labels.append(grid_label)
            relationships = []
            for relationship in inst.get("relationships", []):
                grid_id, _ = models.GridInstitute.objects.get_or_create(
                    grid_id=relationship.get("id"), defaults={"status": "inactive"}
                )
                grid_relationship, _ = models.GridRelationship.objects.update_or_create(
                    grid_id=grid_id,
                    _type=relationship.get("type"),
                    label=relationship.get("label"),
                )
                relationships.append(grid_relationship)
            defaults = {
                "status": inst.get("status"),
                "name": inst.get("name"),
                "wikipedia_url": inst.get("wikipedia_url"),
                "email_address": inst.get("email_address"),
                "established": inst.get("established"),
                "weight": inst.get("weight"),
            }
            grid_institute, _ = models.GridInstitute.objects.update_or_create(
                grid_id=inst.get("id"), defaults=defaults
            )
            grid_institute.links.set(links)
            grid_institute.aliases.set(aliases)
            grid_institute.acronyms.set(acronyms)
            grid_institute.types.set(types)
            grid_institute.ip_addresses.set(ip_addresses)
            grid_institute.addresses.set(addresses)
            grid_institute.labels.set(labels)
            grid_institute.relationships.set(relationships)

            source_external_ids = inst.get("external_ids")
            if source_external_ids:
                external_ids = []
                for identifier, id_obj in source_external_ids.items():
                    external_id, _ = models.GridExternalId.objects.update_or_create(
                        identifier=identifier,
                        preferred=id_obj.get("preferred"),
                        _all=id_obj.get("all"),
                    )
                    external_ids.append(external_id)
                grid_institute.external_ids.set(external_ids)
        return inst.get("id")
