from django.contrib.postgres.fields import ArrayField
from django.db import models

from apps.research_db.models import Country


class GridLink(models.Model):
    name = models.TextField(unique=True)

    def __str__(self):
        return str(self.name)


class GridAlias(models.Model):
    name = models.TextField(unique=True)

    class Meta:
        verbose_name_plural = "Grid Aliases"

    def __str__(self):
        return str(self.name)


class GridAcronym(models.Model):
    name = models.TextField(unique=True)

    def __str__(self):
        return str(self.name)


class GridType(models.Model):
    name = models.TextField(unique=True)

    def __str__(self):
        return str(self.name)


class GridIpAddress(models.Model):
    name = models.TextField(unique=True)

    class Meta:
        verbose_name_plural = "Grid IP Addresses"

    def __str__(self):
        return str(self.name)


class GeonamesAdmin(models.Model):
    name = models.TextField(null=True)
    ascii_name = models.TextField()
    code = models.TextField(unique=True)

    def __str__(self):
        return str(self.name)


class License(models.Model):
    attribution = models.TextField(unique=True)
    license = models.TextField(unique=True)

    def __str__(self):
        return str(self.license)


class GeonamesCity(models.Model):
    geonames_city_id = models.IntegerField(unique=True)
    city = models.TextField()
    nuts_level1 = models.TextField(blank=True, null=True)
    nuts_level2 = models.TextField(blank=True, null=True)
    nuts_level3 = models.TextField(blank=True, null=True)
    geonames_admin = models.ManyToManyField(GeonamesAdmin, blank=True)
    license = models.ForeignKey(License, on_delete=models.PROTECT)

    class Meta:
        verbose_name_plural = "Geonames Cities"

    def __str__(self):
        return str(self.city)


class GridAddress(models.Model):
    line_1 = models.TextField(blank=True, null=True)
    line_2 = models.TextField(blank=True, null=True)
    line_3 = models.TextField(blank=True, null=True)
    lat = models.FloatField(blank=True, null=True)
    lng = models.FloatField(blank=True, null=True)
    postcode = models.TextField(blank=True, null=True)
    primary = models.BooleanField()
    city = models.TextField()
    state = models.TextField(blank=True, null=True)
    state_code = models.TextField(blank=True, null=True)
    country = models.ForeignKey(Country, on_delete=models.PROTECT)
    geonames_city = models.ForeignKey(
        GeonamesCity, on_delete=models.PROTECT, blank=True, null=True
    )

    class Meta:
        verbose_name_plural = "Grid Addresses"

    def __str__(self):
        return ", ".join(
            (
                str(x)
                for x in (
                    self.line_1,
                    self.line_2,
                    self.line_3,
                    self.postcode,
                    self.city,
                    self.state,
                    self.country,
                    f"(Lat: {self.lat}, Lng: {self.lng})",
                )
                if x
            )
        )


class Iso639(models.Model):
    name = models.TextField(unique=True)

    class Meta:
        ordering = ("name",)

    def __str__(self):
        return str(self.name)


class GridLabel(models.Model):
    label = models.TextField()
    iso639 = models.ForeignKey(Iso639, on_delete=models.PROTECT)

    class Meta:
        ordering = ("label",)

    def __str__(self):
        return str(self.label)


class GridRelationship(models.Model):
    _type = models.TextField()
    label = models.TextField()
    grid_id = models.ForeignKey(
        "GridInstitute", to_field="grid_id", on_delete=models.PROTECT
    )

    def __str__(self):
        return f"{self.label} {self._type}"


class GridExternalId(models.Model):
    identifier = models.TextField()
    preferred = models.TextField(null=True)
    _all = ArrayField(models.TextField())

    def __str__(self):
        return f"{self.identifier}: {self._all}"


class GridInstitute(models.Model):
    grid_id = models.CharField(max_length=16, unique=True)
    status = models.CharField(max_length=10, default="internal")
    redirect = models.ForeignKey(
        "self", on_delete=models.PROTECT, blank=True, null=True
    )
    name = models.TextField(null=True)
    wikipedia_url = models.TextField(blank=True, null=True)
    email_address = models.TextField(blank=True, null=True)
    links = models.ManyToManyField(GridLink, blank=True)
    aliases = models.ManyToManyField(GridAlias, blank=True)
    acronyms = models.ManyToManyField(GridAcronym, blank=True)
    types = models.ManyToManyField(GridType, blank=True)
    ip_addresses = models.ManyToManyField(GridIpAddress, blank=True)
    addresses = models.ManyToManyField(GridAddress, blank=True)
    labels = models.ManyToManyField(GridLabel, blank=True)
    established = models.IntegerField(blank=True, null=True)
    relationships = models.ManyToManyField(GridRelationship, blank=True)
    external_ids = models.ManyToManyField(GridExternalId, blank=True)
    weight = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return " | ".join(
            (str(x) for x in (self.grid_id, self.name) if x)
        )


class InternalGridInstitute(GridInstitute):
    class Meta:
        proxy = True
