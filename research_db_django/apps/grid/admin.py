from django.contrib import admin

from apps.grid import models


class NoDeleteAdminMixin:
    def has_delete_permission(self, request, obj=None):
        return False


class NoChangeAdminMixin:
    def has_change_permission(self, request, obj=None):
        return False


class NoAddAdminMixin:
    def has_add_permission(self, request, obj=None):
        return False


@admin.register(models.GridLink)
class GridLinkAdmin(admin.ModelAdmin):
    search_fields = ("name",)


@admin.register(models.GridAlias)
class GridAliasAdmin(admin.ModelAdmin):
    search_fields = ("name",)


@admin.register(models.GridAcronym)
class GridAcronymAdmin(admin.ModelAdmin):
    search_fields = ("name",)


@admin.register(models.GridType)
class GridTypeAdmin(admin.ModelAdmin):
    search_fields = ("name",)


@admin.register(models.GridIpAddress)
class GridIpAddressAdmin(admin.ModelAdmin):
    search_fields = ("name",)


@admin.register(models.GeonamesAdmin)
class GeonamesAdminAdmin(admin.ModelAdmin):
    search_fields = ("name",)


@admin.register(models.License)
class LicenseAdmin(admin.ModelAdmin):
    search_fields = ("attribution", "license")


@admin.register(models.GeonamesCity)
class GeonamesCityAdmin(admin.ModelAdmin):
    pass


@admin.register(models.GridAddress)
class GridAddressAdmin(admin.ModelAdmin):
    search_fields = ("city", "lat", "line_1", "line_2", "line_3", "lng", "postcode", "country")


@admin.register(models.GridLabel)
class GridLabelAdmin(admin.ModelAdmin):
    search_fields = ("label",)
    list_filter = ("iso639",)


@admin.register(models.GridRelationship)
class GridRelationshipAdmin(admin.ModelAdmin):
    search_fields = ("grid_id", "label", "_type",)
    raw_id_fields = ("grid_id",)

@admin.register(models.GridExternalId)
class GridExternalIdAdmin(admin.ModelAdmin):
    search_fields = ("_all",)


@admin.register(models.InternalGridInstitute)
class InternalGridInstituteAdmin(admin.ModelAdmin):
    autocomplete_fields = (
        "redirect",
        "links",
        "aliases",
        "acronyms",
        "types",
        "ip_addresses",
        "labels",
        "relationships",
        "external_ids",
        "addresses",
    )
    list_filter = ("status", "types")
    list_display = ("grid_id", "name", "_addresses", "_labels", "_relationships")
    search_fields = (
        "grid_id",
        "name",
        "aliases__name",
        "acronyms__name",
        "addresses__country__name",
        "addresses__city",
        "addresses__country__alpha_2_code",
        "addresses__country__alpha_3_code",
    )
    exclude = ("status", "redirect")

    def _addresses(self, obj):
        return "| ".join([f"{a.city}, {a.country}" for a in obj.addresses.all()])

    def _labels(self, obj):
        return " | ".join([f"{l.iso639}: {l.label}" for l in obj.labels.all()])

    def _relationships(self, obj):
        return " | ".join([f"{r.label} ({r._type})" for r in obj.relationships.all()])

    def get_form(self, request, obj=None, **kwargs):
        last_internal_grid = models.InternalGridInstitute.objects.filter(
            grid_id__regex=r"internal.[0-9]"
        ).last()
        if last_internal_grid:
            next_internal_id = int(last_internal_grid.grid_id.strip("internal.")) + 1
        else:
            next_internal_id = 1
        form = super(InternalGridInstituteAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields["grid_id"].initial = f"internal.{next_internal_id}"
        return form

    def get_queryset(self, request):
        return self.model.objects.filter(status="internal")


@admin.register(models.GridInstitute)
class GridInstituteAdmin(InternalGridInstituteAdmin):
    exclude = ()

    def get_queryset(self, request):
        return self.model.objects.all()
