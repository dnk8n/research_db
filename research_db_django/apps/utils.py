class PauseSignals:
    def __init__(self, signals_to_pause):
        self.signals_to_pause = signals_to_pause

    def __enter__(self):
        for signal, model_signal, dispatch_uid in self.signals_to_pause:
            signal.disconnect(model_signal, dispatch_uid=dispatch_uid)

    def __exit__(self, type_, value, traceback):
        for signal, model_signal, dispatch_uid in self.signals_to_pause:
            signal.connect(model_signal, dispatch_uid=dispatch_uid)
