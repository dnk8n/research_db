Work in progress, licensed under AGPL-3.0-or-later


## Development Environment

Vagrant was chosen to make development environments as close to production as possible and also to minimise differences
between operating systems. Mkcert is used to allow ssl to be valid on development system browsers (Do not use mkcert outside 
development)

1. Install Direnv - once-off

1. `cp .envrc.tpl .envrc` - once-off

1. Modify .envrc with correct values - once-off

1. `direnv allow .` - once-off, or every time .envrc is modified

1. `ansible-galaxy install -r config/ansible/requirements.yml` - each time requirements change

1. `vagrant plugin install vagrant-hosts` - once-off

1. python-netaddr required on host: `sudo dnf install python-netaddr` or `sudo apt install python-netaddr` - once-off

1. Install mkcert (jump to `build from source (requires Go 1.13+)` of [mkcert docs](https://github.com/FiloSottile/mkcert#linux)) - once-off

1. `mkcert -install` - once-off

1. `cd config/services/traefik/certs`

1. `mkcert -cert-file local.crt -key-file local.key "${DOMAIN}" "*.${DOMAIN}"` - once-off
    Note: If certs are regenerated while traefik is already up, `docker-compose restart traefik`

1. `sudo nano /etc/hosts` and copy in block resulting from the following command:
   ```
   printf "\
   ${VAGRANT_IP} ${DOMAIN}\n\
   ${VAGRANT_IP} ${DOMAIN_DJANGO}\n\
   ${VAGRANT_IP} ${DOMAIN_HASURA}\n\
   ${VAGRANT_IP} ${DOMAIN_TRAEFIK}\n\
   ${VAGRANT_IP} ${DOMAIN_NETDATA}\n\
   ${VAGRANT_IP} ${DOMAIN_PROMETHEUS}\n\
   ${VAGRANT_IP} ${DOMAIN_GRAFANA}\n\
   ${VAGRANT_IP} ${DOMAIN_MAILHOG}\n\
   "
   ```

1. `cd -` (or otherwise navigate to root of repo)

1. `vagrant up`

1. `vagrant ssh`

1. `cd src`

1. `direnv allow .` - each environment variable change

1. `docker-compose build` - each code change

1. `docker-compose up -d postgres`

1. To restore database dump: `gunzip < /path/to/rdb.<timestamp>.pg.dump.sql.gzip | docker-compose exec -T postgres psql -U ${POSTGRES_USER} ${POSTGRES_DB}`
   Note: To create a database dump: `docker-compose exec -T postgres pg_dump -U ${POSTGRES_USER} -O -x ${POSTGRES_DB} | gzip -9 > /path/to/backups/rdb.$(date '+%Y%m%d-%H%M').pg.dump.sql.gzip`

1. `docker-compose run --rm django ./manage.py migrate --run-syncdb`

1. `docker-compose run --rm django ./manage.py collectstatic`

1. `docker-compose run --rm django ./manage.py createsuperuser` - if not already included in dump

1. `docker-compose up -d`

1. In a browser, you can now navigate to the endpoints defined in /etc/hosts
