version: "3.8"

networks:
  traefik:
    name: traefik
  database:
    name: database
  prometheus:
    name: prometheus
  redis:
    name: redis
  netdata-dockerproxy:
    name: netdata-dockerproxy

volumes:
  prometheus:
    name: prometheus
  grafana:
    name: grafana
  static:
    name: static
  database:
    name: database
  netdataconfig:
    name: netdataconfig
  netdatalib:
    name: netdatalib
  netdatacache:
    name: netdatacache

services:
  django:
    container_name: django
    image: ${DJANGO_IMAGE}:${DJANGO_IMAGE_TAG}
    command:
      - gunicorn
      - config.wsgi:app
      - --bind=0.0.0.0:8000
      - --worker-tmp-dir=/dev/shm
      - --workers=2
      - --threads=4
      - --worker-class=gthread
      - --log-file=-
    depends_on:
      - postgres
      - redis
    labels:
      traefik.enable: "true"
      traefik.http.routers.django.entrypoints: websecure
      traefik.http.routers.django.rule: Host(`${DOMAIN_DJANGO}`)
      traefik.http.routers.django.tls: "true"
      traefik.http.services.django.loadbalancer.server.port: 8000
    restart: unless-stopped
    environment:
      DATABASE_URL: ${DATABASE_URL}
      REDIS_URL: ${REDIS_URL}
      EMAIL_HOST: ${EMAIL_HOST}
      EMAIL_PORT: ${EMAIL_PORT}
      EMAIL_HOST_USER: ${EMAIL_HOST_USER}
      EMAIL_HOST_PASSWORD: ${EMAIL_HOST_PASSWORD}
    networks:
      - database
      - redis
      - traefik
    volumes:
      - static:/static
    working_dir: /app

  django-q:
    container_name: django-q
    image: ${DJANGO_IMAGE}:${DJANGO_IMAGE_TAG}
    command: ["/app/manage.py", "qcluster"]
    depends_on:
      - postgres
      - redis
    restart: unless-stopped
    environment:
      DATABASE_URL: ${DATABASE_URL}
      REDIS_URL: ${REDIS_URL}
      EMAIL_HOST: ${EMAIL_HOST}
      EMAIL_PORT: ${EMAIL_PORT}
      EMAIL_HOST_USER: ${EMAIL_HOST_USER}
      EMAIL_HOST_PASSWORD: ${EMAIL_HOST_PASSWORD}
    networks:
      - database
      - redis
    volumes:
      - static:/static
    working_dir: /app  

  redis:
    image: redis:6.0.10-alpine3.13
    container_name: redis
    networks:
      - redis
    restart: unless-stopped

  hasura:
    container_name: hasura
    image: hasura/graphql-engine:v1.3.3.cli-migrations-v2
    command:
      - graphql-engine
      - serve
      - --server-port=5000
    depends_on:
      - postgres
    labels:
      traefik.enable: "true"
      traefik.http.routers.hasura.entrypoints: websecure
      traefik.http.routers.hasura.rule: Host(`${DOMAIN_HASURA}`)
      traefik.http.routers.hasura.tls: "true"
      traefik.http.services.hasura.loadbalancer.server.port: 5000
    restart: unless-stopped
    environment:
      HASURA_GRAPHQL_ENABLE_CONSOLE: "true"
      HASURA_GRAPHQL_DATABASE_URL: ${DATABASE_URL}
    networks:
      - traefik
      - database
    volumes:
      - ./research_db_hasura/metadata:/hasura-metadata

  grafana:
    container_name: grafana
    image: grafana/grafana:7.3.7
    environment:
      GF_INSTALL_PLUGINS: grafana-piechart-panel
    labels:
      traefik.enable: "true"
      traefik.http.routers.grafana.entrypoints: websecure
      traefik.http.routers.grafana.rule: Host(`${DOMAIN_GRAFANA}`)
      traefik.http.routers.grafana.tls: "true"
      traefik.http.services.grafana.loadbalancer.server.port: 3000
    volumes:
      - grafana:/var/lib/grafana:rw
    networks:
      - traefik
      - prometheus
    restart: unless-stopped

  netdata:
    container_name: netdata
    cap_add:
      - SYS_PTRACE
    environment:
      VIRTUALIZATION: ${VIRTUALIZATION}
      DOCKER_HOST: netdata-dockerproxy:2375
    hostname: ${DOMAIN_NETDATA}
    image: netdata/netdata
    labels:
      traefik.enable: "true"
      traefik.http.routers.netdata.entrypoints: websecure
      traefik.http.routers.netdata.rule: Host(`${DOMAIN_NETDATA}`)
      traefik.http.routers.netdata.tls: "true"
      traefik.http.services.netdata.loadbalancer.server.port: 19999
    networks:
      - traefik
      - prometheus
      - netdata-dockerproxy
    depends_on:
      - netdata-dockerproxy
    restart: unless-stopped
    security_opt:
      - apparmor:unconfined
    volumes:
      - netdataconfig:/etc/netdata
      - netdatalib:/var/lib/netdata
      - netdatacache:/var/cache/netdata
      - /etc/passwd:/host/etc/passwd:ro
      - /etc/group:/host/etc/group:ro
      - /proc:/host/proc:ro
      - /sys:/host/sys:ro
      - /etc/os-release:/host/etc/os-release:ro

  netdata-dockerproxy:
    privileged: true
    container_name: netdata-dockerproxy
    image: tecnativa/docker-socket-proxy
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
    environment:
      CONTAINERS: 1
    networks:
      - netdata-dockerproxy
    restart: unless-stopped

  prometheus:
    container_name: prometheus
    image: prom/prometheus:v2.24.1
    labels:
      traefik.enable: "true"
      traefik.http.routers.prometheus.entrypoints: websecure
      traefik.http.routers.prometheus.rule: Host(`${DOMAIN_PROMETHEUS}`)
      traefik.http.routers.prometheus.tls: "true"
      traefik.http.services.prometheus.loadbalancer.server.port: 9090
    networks:
      - traefik
      - prometheus
    restart: unless-stopped
    volumes:
      - ./config/services/prometheus:/etc/prometheus
      - prometheus:/prometheus
    command:
      - --web.enable-lifecycle
      - --config.file=/etc/prometheus/prometheus.yml

  postgres:
    container_name: postgres
    image: postgres:13.1-alpine
    environment:
      POSTGRES_USER: ${POSTGRES_USER}
      POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}
      POSTGRES_DB: ${POSTGRES_DB}
    networks:
      - database
    volumes:
      - database:/var/lib/postgresql/data
    restart: unless-stopped

  traefik:
    container_name: traefik
    image: traefik:v2.3.6
    labels:
      traefik.enable: "true"
      traefik.http.routers.traefik.entrypoints: websecure
      traefik.http.routers.traefik.rule: Host(`${DOMAIN_TRAEFIK}`)
      traefik.http.routers.traefik.service: api@internal
      traefik.http.routers.traefik.tls: "true"
      traefik.http.services.traefik.loadbalancer.server.port: 8080
    networks:
      - traefik
      - prometheus
    ports:
      - 80:80
      - 443:443
    restart: unless-stopped
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
